# -*- coding: utf-8 -*-
from .base_settings import *
from .settings_pieces import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'kmut',
        'USER': 'kmut',
        'PASSWORD': 'kmut',

        'HOST': 'localhost',
        'PORT': '5432',
        'AUTOCOMMIT': True,
    },
}
INSTALLED_APPS += (
    'debug_toolbar',
)

SECRET_KEY = 'gl=4_txhkdx7w327r&l5gr1!*od2l4f*iz8c3b+us9eiw0nc_i'

INTERNAL_IPS = ['127.0.0.1']

