# -*- coding: utf-8 -*-
from .base_settings import *
from .settings_pieces import *

DEBUG = True

if DEBUG:
    from settings_pieces.debug_toolbar_settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'kmut_db',
        'USER': 'kmut',
        'PASSWORD': 'kmut',

        'HOST': 'localhost',
        'PORT': '5432',
        'AUTOCOMMIT': True,
    },
}

INTERNAL_IPS = ['127.0.0.1']
