# -*- coding: utf-8 -*-
try:
    from .local_settings import *
except ImportError, err:
    print err
    from .base_settings import *
    from .settings_pieces import *
