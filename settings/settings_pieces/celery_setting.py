# -*- coding: utf-8 -*-
import djcelery
from kombu import Queue

from ..base_settings import INSTALLED_APPS

INSTALLED_APPS += ('djcelery',)

djcelery.setup_loader()


CELERYD_LOG_FORMAT = '[%(asctime)s: %(levelname)s/%(processName)s] %(module)s %(message)s'
CELERYD_TASK_LOG_FORMAT = '[%(asctime)s: %(levelname)s/%(processName)s][%(task_name)s(%(task_id)s)] %(module)s %(message)s'

BROKER_URL = 'redis://localhost:6379/0'

CELERY_RESULT_BACKEND = BROKER_URL

CELERY_DEFAULT_QUEUE = 'default'


CELERY_QUEUES = (
    Queue('default', routing_key='task.#'),
    Queue('slow', routing_key='slow.#'),
)

# CELERY_DEFAULT_EXCHANGE = 'tasks'
# CELERY_DEFAULT_EXCHANGE_TYPE = 'topic'
# CELERY_DEFAULT_ROUTING_KEY = 'task.default'


CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

CELERYBEAT_SCHEDULE = {
}

CELERY_TIMEZONE = 'UTC+03'


CELERYD_PREFETCH_MULTIPLIER = 0
CELERY_DISABLE_RATE_LIMITS = True
CELERY_DB_REUSE_MAX = 100

