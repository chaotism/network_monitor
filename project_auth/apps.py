from __future__ import unicode_literals

from django.apps import AppConfig


class KmutAuthConfig(AppConfig):
    name = 'project_auth'
