# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from rest_framework import serializers, pagination


USER_URL_PK = 'user_pk'
USER_MODEL = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}, 'email': {'required': False}}

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.is_active = True
        user.set_password(validated_data['password'])
        user.save()
        return user