# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import UserViewSet

tokens_urls = [
    url(r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
    url(r'^api-token-refresh/', 'rest_framework_jwt.views.refresh_jwt_token'),
    url(r'^api-token-verify/', 'rest_framework_jwt.views.verify_jwt_token'),
]

users_urls = [
    url(r'^$', UserViewSet.as_view({'get': 'list', 'post': 'create'}), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', UserViewSet.as_view({'get': 'retrieve','put': 'update','patch': 'partial_update','delete': 'destroy'}), name='detail'),
]

urlpatterns = [
    url(r'^tokens/', include(tokens_urls, namespace='tokens')),
    url(r'^users/', include(users_urls, namespace='users')),
]

