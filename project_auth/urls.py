# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url


urlpatterns = [
    url(r'^api/', include('project_auth.api.urls', namespace='api')),
]
