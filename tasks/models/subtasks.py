# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models.query import Q
from django.db.models.signals import pre_save, post_save
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.dispatch import receiver
from ..choices import ATTRIBUTE_VALUE_TYPES, ATTRIBUTE_TYPES  , OPER_TYPES, STATUS_TYPES, ERROR_TYPES
from .utils import sub_task_starter

APP_LABEL = 'tasks'

logger = logging.getLogger(__name__)


class Subtask(models.Model):
    """
    Подзадача.
    """
    STATUS_TYPES = STATUS_TYPES
    ERROR_TYPES = ERROR_TYPES

    task = models.ForeignKey('Task', related_name='subtasks', db_index=True, on_delete=models.CASCADE, verbose_name=_('task'))
    create_time = models.DateTimeField(auto_now_add=True, verbose_name=_('create time'))
    start_time = models.DateTimeField(blank=True, null=True, verbose_name=_('start time'))
    finish_time = models.DateTimeField(blank=True, null=True, verbose_name=_('finish time'))
    celery_uuid = models.CharField(max_length=128, blank=True, null=True,  verbose_name=_('celery uuid'))
    status_type = models.CharField(max_length=40, choices=STATUS_TYPES, editable=False, default=STATUS_TYPES.IDLE, verbose_name=_('status type'))
    error_type = models.CharField(max_length=40, choices=ERROR_TYPES, blank=True, null=True, editable=False, verbose_name=_('error type'))
    progress = models.PositiveIntegerField(blank=True, null=True, default=None, verbose_name=_('progress'))

    class Meta:
        app_label = APP_LABEL
        ordering = ('pk',)
        verbose_name = _('subtask')
        verbose_name_plural = _('subtasks')

    def __unicode__(self):
        return 'subtask %s status %s celery_uuid %s' % (self.id, self.status_type, self.celery_uuid)

    def mark_completed(self, commit=True):
        self.progress = 100
        self.error_type = None
        self._set_status(self.STATUS_TYPES.COMPLETED, commit=commit)

    def mark_skipped(self, commit=True):
        self.progress = None
        self.error_type = None
        self._set_status(self.STATUS_TYPES.SKIPPED, commit=commit)

    def mark_failed(self, error_type, commit=True):
        self.error_type = error_type
        self._set_status(self.STATUS_TYPES.FAILED, commit=commit)

    def mark_running(self, commit=True):
        self.start_time = timezone.now()
        self.progress = 0
        self._set_status(self.STATUS_TYPES.RUNNING, commit=commit)

    def mark_idle(self, commit=True):
        self.error_type = None
        self.progress = None
        self._set_status(self.STATUS_TYPES.IDLE, commit=commit)

    def mark_paused(self, commit=True):
        self.error_type = None
        self._set_status(self.STATUS_TYPES.PAUSED, commit=commit)

    def mark_planned(self, celery_uuid=None, commit=True):
        if not celery_uuid:
            raise Exception('Debug: celery uuid is empty')
        self.error_type = None
        self.progress = None
        self.celery_uuid = celery_uuid
        self._set_status(self.STATUS_TYPES.PLANNED, commit=commit)

    def _set_status(self, status_type, commit=True):
        old_state = self.status_type
        self.status_type = status_type
        if commit:
            self.save()

    def set_progress(self, progress, commit=True):
        self.progress = progress
        if commit:
            self.save()

    @property
    def can_be_started(self):
        """
        Можно запустить подзадачу:
        - если подзадача не запланирована или приостановлена.
        """
        return True  # TODO: убрать
        if self.status_type in [self.STATUS_TYPES.IDLE, self.STATUS_TYPES.PAUSED]:
            return True
        return False

    def start(self):
        """
        Запуск подзадачи.
        """
        return sub_task_starter(self)

    def stop(self):
        """
        Oстановка подзадачи.
        """
        raise NotImplementedError('not implemented')
        #TODO: нужно реализовать на api http://celery.readthedocs.org/en/latest/userguide/remote-tasks.html#guide-webhooks
        #
        #result = add.apply_async(args=[2, 2], countdown=120)
        #result.revoke()

        #or if you only have the task id:

        #from proj.celery import app
        #app.control.revoke(task_id)
        # return sub_task_stopper(self)

    @property
    def can_be_cancelled(self):
        """
        Можно остановить подзадачу:
        - если подзадача не запланирована или
        """
        status_type = self.status_type
        if status_type in [self.STATUS_TYPES.IDLE]:
            return True
        return False

    def cancel(self):
        """
        Отмена подзадачи.
        """
        if self.can_be_cancelled:
            self.stop()
        logger.info('subtask %s stopped' % self)

    def get_results(self):
        # TODO: переделать через prefetch related
        return SubtaskResult.objects.filter(subtask=self).values('result', 'date',)

    def get_errors(self):
        return SubtaskResult.objects.filter(subtask=self).values('error', 'date',)


class SubtaskResult(models.Model):
    subtask = models.ForeignKey('Subtask', related_name='subtask_results', db_index=True, on_delete=models.CASCADE, verbose_name=_('subtask'))
    date = models.DateTimeField(auto_now_add=True, verbose_name=_('data_time'))
    result = JSONField(blank=True, null=True, verbose_name=_('result'))
    error = JSONField(blank=True, null=True, verbose_name=_('error'),)

    class Meta:
        app_label = APP_LABEL
        ordering = ('pk',)
        verbose_name = _('subtask result')
        verbose_name_plural = _('subtasks results')

    def __unicode__(self):
        return '%s %s %s' % (self.id, self.subtask, self.result)


@receiver(pre_save, sender=Subtask)
def give_default_status(sender, instance, *args, **kwargs):
    if not instance.status_type:
        instance.status_type = STATUS_TYPES.IDLE

@receiver(post_save, sender=Subtask)
def start_subtask(sender, instance, *args, **kwargs):
    if not instance.celery_uuid:
        instance.start()