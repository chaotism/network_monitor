# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Avg, Max, Min
from django.db.models.query import Q
from django.db.models.signals import pre_save, post_save
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.conf import settings
from django.dispatch import receiver
from network_devices.models import NetworkDevice  # TODO: связанность с net_work_devices, можно обойти с помощью GenericForeignKey

from ..choices import ATTRIBUTE_VALUE_TYPES, ATTRIBUTE_TYPES  , OPER_TYPES, STATUS_TYPES, ERROR_TYPES
from .utils import subtask_creator
from .subtasks import SubtaskResult


APP_LABEL = 'tasks'

logger = logging.getLogger(__name__)


class Task(models.Model):
    """
    Задача.
    """
    OPER_TYPES = OPER_TYPES

    user = models.ForeignKey(User, blank=True, null=True, db_column='user_id', db_index=True, on_delete=models.SET_NULL, verbose_name=_('user'), )
    description = models.CharField(max_length=400, blank=True, null=True, verbose_name=_('description'))
    operation_type = models.CharField(max_length=40, choices=OPER_TYPES, verbose_name=_('operation type'))
    network_device = models.ForeignKey(NetworkDevice, related_name='tasks', db_index=True, on_delete=models.CASCADE, verbose_name=_('network device'))

    class Meta:
        app_label = APP_LABEL
        ordering = ('pk',)
        verbose_name = _('task')
        verbose_name_plural = _('tasks')

    def __unicode__(self):
        return '%s %s' % (self.pk, self.description)

    @property
    def count_completed_subtasks(self):
        codes = STATUS_TYPES
        return self.children_tasks.filter(status_type__in=[codes.COMPLETED, codes.FAILED]).count()

    def start(self):
        """
        Запуск задачи.
        Возвращает 2 списка:
        - список успешно запущенных подзадач
        - список словарей: {subtask: подзадача, error: исключение}
        """
        started = []
        errors = []

        for subtask in subtask_creator(self):
            try:
                subtask.start()
                started.append(subtask)
            except Exception, e:
                errors.append({
                    'subtask': subtask,
                    'error': e
                })
        return started, errors

    def cancel(self):
        """
        Отмена задачи.
        Возвращает 2 списка:
        - список успешно отмененных подзадач
        - список словарей: {subtask: подзадача, error: исключение}
        """
        cancelled = []
        errors = []  # TODO
        for subtask in self.subtask_set.all():
            try:
                subtask.cancel()
                cancelled.append(subtask)
            except Exception, err:
                errors.append({
                    'subtask': subtask,
                    'error': err
                })
        return cancelled, errors

    def get_start_time(self):
        return self.subtasks.aggregate(start_time=Min('start_time'))['start_time']

    def get_finish_time(self):
        return self.subtasks.aggregate(finish_time=Max('finish_time'))['finish_time']

    def get_results(self):
        # TODO: переделать через prefetch related
        return SubtaskResult.objects.filter(subtask__task=self).exclude(Q(result='')|Q(result__isnull=True)).values('result', 'date', )

    def get_errors(self):
        return SubtaskResult.objects.filter(subtask__task=self).exclude(Q(error='')|Q(error__isnull=True)).values('error', 'date', )


class TaskAttribute(models.Model):
    """
    Атрибут задачи.
    """
    ATTRIBUTE_TYPES = ATTRIBUTE_TYPES
    ATTRIBUTE_VALUE_TYPES = ATTRIBUTE_VALUE_TYPES

    task = models.ForeignKey('Task', related_name='task_attributes', db_index=True, verbose_name=_('task'))
    type = models.CharField(max_length=40, choices=ATTRIBUTE_TYPES, verbose_name=_('type'))
    value_type = models.CharField(max_length=40, choices=ATTRIBUTE_VALUE_TYPES, default=ATTRIBUTE_VALUE_TYPES.STRING, verbose_name=_('attr_code'))
    value = models.CharField(max_length=2000, verbose_name=_('value'))

    class Meta:
        app_label = APP_LABEL
        ordering = ('pk',)
        verbose_name = _('task attribute')
        verbose_name_plural = _('task attributes')

    def __unicode__(self):
        return '%s %s %s' % (self.value, self.task, self.type)


@receiver(post_save, sender=Task)
def start_task(sender, instance, *args, **kwargs):
    if not instance.subtasks.count():
        instance.start()
