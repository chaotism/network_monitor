# -*- coding: utf-8 -*-
from __future__ import absolute_import
import logging
import importlib
from django.utils.translation import ugettext_lazy as _
from ...choices import STATUS_TYPES


logger = logging.getLogger(__name__)

# TODO: переделать
TASK_OPERATION = {
    'NetworkPerfomanceCheck': {
        'function': 'tasks.algos.network_perfomance_check',
        'count_subtask':1
    }
}


def subtask_creator(task):
    operation_code = task.operation_type
    operation_dict = TASK_OPERATION.get(operation_code)
    count_subtask = operation_dict.get('count_subtask')
    return [task.subtasks.create(task=task, status_type=STATUS_TYPES.IDLE) for i in range(count_subtask)]


def sub_task_starter(sub_task):
    if not sub_task.can_be_started:
        raise Exception(_("Subtask %s can't be started" % sub_task.pk))
    operation_code = sub_task.task.operation_type
    operation_dict = TASK_OPERATION.get(operation_code)
    function = get_function_from_str(operation_dict.get('function'))

    if not function:
        raise Exception(_('Cannot find function to this task. Oper code: %s' % operation_code))
    try:
        result = function.delay(sub_task.pk)
    except Exception, err:
        logger.error(err)
        raise
    if hasattr(result, 'task_id'):
        celery_uuid = result.task_id
    else:
        celery_uuid = None
    if celery_uuid:
        sub_task.mark_planned(celery_uuid=celery_uuid)
    logger.info('subtask %s started' % sub_task)
    return sub_task


def get_function_from_str(function_str):
    logger.info('take function from str')
    mod_name, function_name = function_str.rsplit('.', 1)
    try:
        logger.debug('mod_name: %s' % mod_name)
        logger.debug('function_name: %s' % function_name)
        mod = importlib.import_module(mod_name)
        function = getattr(mod, function_name)
    except (ImportError, AttributeError), err:
        logger.error(err)
        raise
    return function