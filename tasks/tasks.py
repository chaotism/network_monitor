# coding: utf-8
from __future__ import unicode_literals
import logging


logger = logging.getLogger(__name__)


try:
    from .algos import network_perfomance_check
except Exception, err:
    logger.error(str(err))
    raise
