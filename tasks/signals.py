from django.dispatch import Signal


status_changed = Signal(providing_args=["instance", "from_state"])
