# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import *


#automatic register all models from models
for name in locals().values():
    try:
        admin.site.register(name)
    except:
        pass