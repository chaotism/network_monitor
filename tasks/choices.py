# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from extended_choices import Choices
from django.utils.translation import ugettext_lazy as _


OPER_TYPES = Choices(
    ('NETWORK_PERFOMANCE_CHECK', 'NetworkPerfomanceCheck', _('NetworkPerfomanceCheck')),
)

ATTRIBUTE_TYPES = Choices(
    ('REPEATS', 'repeats', _('repeats')),
    ('DELAY', 'delay', _('delay')),
    ('COMMAND_ARG', 'command_arg', _('command_arg'))
)

ATTRIBUTE_VALUE_TYPES = Choices(
    ('BOOLEAN', 'boolean', _('boolean')),
    ('INTEGER', 'integer', _('integer')),
    ('REAL', 'real', _('real')),
    ('STRING', 'string', _('string')),
    ('ENUM', 'enum', _('enum')),
    ('FILE', 'file', _('file')),
    ('DATETIME', 'datetime', _('datetime')),
    ('TEXT', 'text', _('text')),
    ('BLOB', 'blob', _('blob')),
)

STATUS_TYPES = Choices(
    ('IDLE', 'idle', _('idle')),
    ('RUNNING', 'running', _('running')),
    ('COMPLETED', 'completed', _('completed')),
    ('PAUSED', 'paused', _('paused')),
    ('FAILED', 'failed', _('failed')),
    ('PLANNED', 'planned', _('planned')),
    ('SKIPPED', 'skipped', _('skipped')),
    ('STUCK', 'stuck', _('stuck')),
)

ERROR_TYPES = Choices(
    ('COMMON', 'common', _('common')),
    ('VALUE', 'value', _('value')),
    ('SYSTEM', 'system', _('system')),
    ('FORMAT', 'format', _('format')),
    ('ATTRIBUTE', 'attribute', _('attribute')),
    ('INOUT', 'inout', _('inout')),
    ('ALGO', 'algo', _('algo')),
    ('DUPLICATE', 'duplicate', _('duplicate')),
)
