# -*- coding: utf-8 -*-
from __future__ import absolute_import
import os
import sys
import time
import logging
import subprocess
import signal
import copy
from datetime import datetime
from celery.task import task, periodic_task
from django.conf import settings
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from .utils import StateUpdater, SkipTask, FormatError, InOutError, AttrError, AlgoError, DuplicateError

from ..models import Subtask, SubtaskResult, TaskAttribute
from ..choices import STATUS_TYPES, ERROR_TYPES, OPER_TYPES, ATTRIBUTE_TYPES, ATTRIBUTE_VALUE_TYPES


logger = logging.getLogger(__name__)


# TODO: Переделать задачи, с возможностью запуска в режиме крон/демон, остановкой и запуском. Сделать подзадачу запускающую  nuttcp -S на хосте


@task
def network_perfomance_check(subtask_pk):
    try:
        try:
            subtask = Subtask.objects.get(pk=subtask_pk)
        except Subtask.DoesNotExist:
            raise ValueError("Subtask %s not found; data corrupted" % subtask_pk)
        task = subtask.task
        mashine_ip = task.network_device.ip_address
        if not task:
            raise ValueError("Task not found; data corrupted")
        try:
            repeats = TaskAttribute.objects.get(task_id=task.id, type=ATTRIBUTE_TYPES.REPEATS).value
            repeats = int(repeats)
            if repeats < 1:
                repeats = 1
        except (ObjectDoesNotExist, MultipleObjectsReturned), err:
            logger.error("cannot find repeat value")
            logger.error(str(err))
            repeats = 1
        try:
            delay = TaskAttribute.objects.get(task_id=task.id, type=ATTRIBUTE_TYPES.DELAY).value
            delay = int(delay)
            if delay < 1:
                delay = 1
        except (ObjectDoesNotExist, MultipleObjectsReturned), err:
            logger.error("cannot find repeat value")
            logger.error(str(err))
            delay = 1
        subtask.start_time = datetime.now()
        logger.debug('NetworkPerfomanceCheck subtask started\n' )
        logger.debug('repeats  %s' % repeats)
        logger.debug('delay  %s' % delay)
        logger.debug(datetime.now())
        subtask.mark_running()
        # command =  """echo `date +'%d/%m/%Y,%H:%M:%S'`,` nuttcp -w1mi %s | awk '{print $1 "," $4 "," $7 "," $9 "," $11 "," $15}'`"""  % mashine_ip
        command = """nuttcp -w1mi %s | awk '{print $1 "," $4 "," $7 "," $9 "," $11 "," $15}' """ % mashine_ip # TODO: вынести в task_attr попробовать растащить через celery chain
        # headers = "date, MB,sec,Mbps,TX,RX,msRTT"
        headers = "MB,sec,Mbps,TX,RX,msRTT"

        for i in xrange(repeats):
            time.sleep(delay)
            applEnv = copy.deepcopy(os.environ)
            # nut_tcp_command_str = "nuttcp -w1mi %s" % mashine_ip
            # awk_filter_command_str = " awk '{print $1 "," $4 "," $7 "," $9 "," $11 "," $15}' "
            # nut_tcp_command = subprocess.Popen(nut_tcp_command_str, stdout=subprocess.PIPE)
            # output = subprocess.check_output(awk_filter_command_str, stdin=nut_tcp_command.stdout)
            # nut_tcp_command.wait()
            # proc = subprocess.Popen(command, executable='nuttcp', stdout=subprocess.PIPE, stderr=subprocess.PIPE,  env=applEnv, shell=True)
            proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE,  env=applEnv, shell=True, preexec_fn=os.setsid)
            result, error = proc.communicate()
            time.sleep(1)
            #proc.terminate()
            #os.killpg(os.getpgid(proc.pid), signal.SIGTERM)

            if result:
                result = dict(zip(headers.split(','), result.strip().split(',')))
            else:
                result = None
            if error:
                error = {'error':'Process crushed with errors %s' % error}
            else:
                error = None
            subtask_result = SubtaskResult.objects.create(subtask=subtask, result=result, error=error)

    except ValueError, err:
        subtask.mark_failed(ERROR_TYPES.VALUE)
        logger.error(err)
        raise
    except InOutError, err:
        subtask.mark_failed(ERROR_TYPES.INOUT)
        logger.error(err)
        raise
    except AttrError, err:
        subtask.mark_failed(ERROR_TYPES.ATTRIBUTE)
        logger.error(err)
        raise
    except FormatError, err:
        subtask.mark_failed(ERROR_TYPES.FORMAT)
        logger.error(err)
        raise
    except AlgoError, err:
        subtask.mark_failed(ERROR_TYPES.ALGO)
        logger.error(err)
        raise
    except Exception, err:
        subtask.mark_failed(ERROR_TYPES.SYSTEM)
        logger.error(err)
        raise
    else:
        subtask.mark_completed()
    finally:
        subtask.finish_time = datetime.now()
        subtask.save()
        return serializers.serialize("json", [subtask, ])