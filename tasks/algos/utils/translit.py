# -*- coding: utf-8 -*-
import string

def translit1(string):
    """ This function works just fine """
    capital_letters = {
        u'а': u'A',
        u'б': u'B',
        u'в': u'V',
        u'г': u'G',
        u'д': u'D',
        u'е': u'E',
        u'ё': u'E',
        u'ж': u'Zh',
        u'з': u'Z',
        u'и': u'I',
        u'й': u'Y',
        u'к': u'K',
        u'л': u'L',
        u'м': u'M',
        u'н': u'N',
        u'о': u'O',
        u'п': u'P',
        u'р': u'R',
        u'с': u'S',
        u'т': u'T',
        u'у': u'U',
        u'ф': u'F',
        u'х': u'H',
        u'ц': u'Ts',
        u'ч': u'Ch',
        u'ш': u'Sh',
        u'щ': u'Sch',
        u'ь': u'',
        u'ы': u'Y',
        u'ъ': u'',
        u'э': u'E',
        u'ю': u'Yu',
        u'я': u'Ya'
    }

    lower_case_letters = {
        u'А': u'a',
        u'Б': u'b',
        u'В': u'v',
        u'Г': u'g',
        u'Д': u'd',
        u'Е': u'e',
        u'Ё': u'e',
        u'Ж': u'zh',
        u'З': u'z',
        u'И': u'i',
        u'Й': u'y',
        u'К': u'k',
        u'Л': u'l',
        u'М': u'm',
        u'Н': u'n',
        u'О': u'o',
        u'П': u'p',
        u'Р': u'r',
        u'С': u's',
        u'Т': u't',
        u'У': u'u',
        u'Ф': u'f',
        u'Х': u'h',
        u'Ц': u'ts',
        u'Ч': u'ch',
        u'Ш': u'sh',
        u'Щ': u'sch',
        u'Ь': u'',
        u'Ы': u'y',
        u'Ъ': u'',
        u'Э': u'e',
        u'Ю': u'yu',
        u'Я': u'ya'
    }

    translit_string = ""

    for index, char in enumerate(string):
        if char in lower_case_letters.keys():
            char = lower_case_letters[char]
        elif char in capital_letters.keys():
            char = capital_letters[char]
            if len(string) > index+1:
                if string[index+1] not in lower_case_letters.keys():
                    char = char.upper()
            else:
                char = char.upper()
        translit_string += char

    return translit_string
