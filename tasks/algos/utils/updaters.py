# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime


class StateUpdater(object):
    def __init__(self, celeryTask, subtask):
        self.celeryTask = celeryTask
        self.subtask = subtask
        self.logstring = ''

    def setProgress(self, current, remaining=0):
        current = int(current)
        if self.subtask.progress != current:
            self.subtask.progress = current
            self.subtask.save(update_fields=['progress'])

    def log(self, message):
        self.logstring += '%s: %s\n' % (datetime.now(), message)
