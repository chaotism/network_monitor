# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class SkipTask(Exception):
    pass


class FormatError(Exception):
    def __init__(self, msg):
        self.msg = msg
        Exception.__init__(self, msg)

    def __str__(self):
        return self.msg


class InOutError(Exception):
    def __init__(self, msg):
        self.msg = msg
        Exception.__init__(self, msg)

    def __str__(self):
        return self.msg


class AttrError(Exception):
    def __init__(self, msg):
        self.msg = msg
        Exception.__init__(self, msg)

    def __str__(self):
        return self.msg


class AlgoError(Exception):
    def __init__(self, msg, code=None):
        self.msg = msg
        self.code = code
        Exception.__init__(self, msg, code)

    def __str__(self):
        return self.msg


class DuplicateError(Exception):
    def __init__(self, msg):
        self.msg = msg
        Exception.__init__(self, msg)

    def __str__(self):
        return self.msg
