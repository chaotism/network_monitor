# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from django.shortcuts import Http404
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.contrib.auth.models import User, AnonymousUser
from rest_framework import generics, status, viewsets, permissions, filters
from rest_framework.decorators import detail_route

from ..serializers import TaskSerializer, TaskAttributeSerializer, SubtaskSerializer
from ...models import Task, Subtask, TaskAttribute
from ..filters import TaskFilter, SubtaskFilter


logger = logging.getLogger(__name__)


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = (permissions.AllowAny,)  # TODO: Поправить permissions
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TaskFilter

    def perform_create(self, serializer):
        task = serializer.save()
        if not task.user and self.request.user:
            if not isinstance(self.request.user, AnonymousUser):
                task.user = self.request.user
                task.save()
        return task


class SubTaskViewSet(viewsets.ModelViewSet):
    queryset = Subtask.objects.all()
    serializer_class = SubtaskSerializer
    permission_classes = (permissions.AllowAny,)  #  TODO: Поправить permissions
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = SubtaskFilter

class TaskAttributeViewSet(viewsets.ModelViewSet):

    queryset = TaskAttribute.objects.all()
    serializer_class = TaskAttributeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('task',)
