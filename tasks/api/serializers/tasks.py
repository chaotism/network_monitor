# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from ...models import Task, Subtask, TaskAttribute
from django.utils.translation import ugettext as _
from rest_framework import serializers, pagination


class TaskAttributeSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskAttribute
        fields = ('id', 'task', 'type', 'value_type', 'value')


class TaskSerializer(serializers.ModelSerializer):
    task_attributes = TaskAttributeSerializer(many=True, required=False, allow_empty=True,  partial=True)
    subtasks = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    results = serializers.JSONField(source='get_results', read_only=True)
    errors = serializers.JSONField(source='get_errors', read_only=True)
    start_time = serializers.JSONField(source='get_start_time', read_only=True)
    finish_time = serializers.JSONField(source='get_finish_time', read_only=True)

    class Meta:
        model = Task
        fields = ('id', 'operation_type', 'user', 'network_device', 'description', 'start_time', 'finish_time', 'task_attributes', 'subtasks', 'results', 'errors')

    def __init__(self, *args, **kwargs):
        super(TaskSerializer, self).__init__(*args, **kwargs)
        self.fields['task_attributes'].child.fields['task'].read_only = True
        self.fields['task_attributes'].child.fields['task'].required = False

    def create(self, validated_data):
        task_attributes_data = validated_data.pop('task_attributes')
        task = Task.objects.create(**validated_data)
        for task_attribute_data in task_attributes_data:
            TaskAttribute.objects.create(task=task, **task_attribute_data)
        return task


class SubtaskSerializer(serializers.ModelSerializer):
    results = serializers.JSONField(source='get_results', read_only=True)
    errors = serializers.JSONField(source='get_errors', read_only=True)

    class Meta:
        model = Subtask
        fields = ('id', 'task', 'create_time', 'start_time', 'finish_time', 'celery_uuid', 'status_type', 'error_type', 'progress', 'results', 'errors')




