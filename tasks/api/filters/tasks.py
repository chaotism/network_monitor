# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import django_filters
from rest_framework import filters
from ...models import Task, Subtask, TaskAttribute
from ...choices import OPER_TYPES, STATUS_TYPES, ERROR_TYPES


class TaskFilter(filters.FilterSet):
    operation_type = django_filters.ChoiceFilter(choices=OPER_TYPES.CHOICES)

    class Meta:
        model = Task
        fields = ('user', 'network_device', 'operation_type', 'subtasks__start_time', 'subtasks__finish_time')


class SubtaskFilter(filters.FilterSet):
    status_type = django_filters.ChoiceFilter(choices=STATUS_TYPES.CHOICES)
    error_type = django_filters.ChoiceFilter(choices=ERROR_TYPES.CHOICES)

    class Meta:
        model = Subtask
        fields = ('task', 'task__network_device', 'start_time', 'finish_time', 'status_type', 'error_type')