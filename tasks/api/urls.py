# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import TaskViewSet, SubTaskViewSet, TaskAttributeViewSet


# TODO: Возможно лучше заменить роутами?
tasks_urls = [
    url(r'^$', TaskViewSet.as_view({'get': 'list', 'post': 'create'}), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', TaskViewSet.as_view({'get': 'retrieve','put': 'update','patch': 'partial_update','delete': 'destroy'}), name='detail'),
]

tasks_attribute_urls = [
    url(r'^$', TaskAttributeViewSet.as_view({'get': 'list', 'post': 'create'}), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', TaskAttributeViewSet.as_view({'get': 'retrieve','put': 'update','patch': 'partial_update','delete': 'destroy'}), name='detail'),
]

sub_tasks_urls = [
    url(r'^$', SubTaskViewSet.as_view({'get': 'list', 'post': 'create'}), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', SubTaskViewSet.as_view({'get': 'retrieve','put': 'update','patch': 'partial_update','delete': 'destroy'}), name='detail'),
]

urlpatterns = [
    url(r'^tasks/', include(tasks_urls, namespace='tasks')),
    url(r'^subtasks/', include(sub_tasks_urls, namespace='subtasks')),
    url(r'^task_attributes/', include(tasks_attribute_urls, namespace='task_attributes')),
]

