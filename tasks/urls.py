# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url


urlpatterns = [
    url(r'^api/', include('tasks.api.urls', namespace='api')),
]
