# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.db.models.query import QuerySet
from .choices import TASK_STATUS_TYPES


class SubtaskQuerySet(QuerySet):

    def filter_by_status(self, status_type):
        return self.filter(status_code=status_type)

    # Status types
    def idle(self):
        return self.filter_by_status(TASK_STATUS_TYPES.IDLE)

    def running(self):
        return self.filter_by_status(TASK_STATUS_TYPES.RUNNING)

    def completed(self):
        return self.filter_by_status(TASK_STATUS_TYPES.COMPLETED)

    def paused(self):
        return self.filter_by_status(TASK_STATUS_TYPES.PAUSED)

    def failed(self):
        return self.filter_by_status(TASK_STATUS_TYPES.FAILED)

    def planned(self):
        return self.filter_by_status(TASK_STATUS_TYPES.PLANNED)

    def skipped(self):
        return self.filter_by_status(TASK_STATUS_TYPES.SKIPPED)

    # Groups of status types
    def pre_started(self):
        """ Задача не была запущена. """
        return self.filter(status_code__in=[
            TASK_STATUS_TYPES.IDLE,
            TASK_STATUS_TYPES.PLANNED
        ])

    def finished(self):
        """ Задача завершена с результатом или без. """
        return self.filter(status_code__in=[
            TASK_STATUS_TYPES.COMPLETED,
            TASK_STATUS_TYPES.FAILED,
            TASK_STATUS_TYPES.PAUSED,
            TASK_STATUS_TYPES.STUCK,
            TASK_STATUS_TYPES.SKIPPED
        ])

    def active(self):
        """ Задача запущена или будет запущена (поставлена в очередь). """
        return self.filter(status_code__in=[
            TASK_STATUS_TYPES.RUNNING,
            TASK_STATUS_TYPES.PLANNED
        ])

    def count(self):
        where = self.query.where
        model_cls = models.get_model(app_label='task', model_name='subtaskcount')
        # TODO: вынести в кэш
        if model_cls and len(where) == 1:
            child = self.query.where.children[0]
            if child[1] == 'exact' and child[0].alias == 'subtask' and child[0].col == 'task_id':
                try:
                    stat = model_cls.objects.get(task_id=child[3])
                except (model_cls.DoesNotExist, model_cls.MultipleObjectsReturned):
                    stat = None
                if stat:
                    return stat.total_count
        return super(SubtaskQuerySet, self).count()


class SubtaskManager(models.Manager):
    use_for_related_fields = True

    def get_queryset(self):
        return SubtaskQuerySet(self.model, using=self._db)

    # Status types
    def idle(self):
        return self.get_queryset().idle()

    def running(self):
        return self.get_queryset().running()

    def completed(self):
        return self.get_queryset().completed()

    def paused(self):
        return self.get_queryset().paused()

    def failed(self):
        return self.get_queryset().failed()

    def planned(self):
        return self.get_queryset().planned()

    def skipped(self):
        return self.get_queryset().skipped()

    # Groups of status types
    def pre_started(self, *args, **kwargs):
        return self.get_queryset().pre_started()

    def finished(self, *args, **kwargs):
        return self.get_queryset().finished()

    def active(self, *args, **kwargs):
        return self.get_queryset().active()
