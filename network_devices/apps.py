from __future__ import unicode_literals

from django.apps import AppConfig


class NetworkDevicesConfig(AppConfig):
    name = 'network_devices'
