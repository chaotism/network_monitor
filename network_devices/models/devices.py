# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from django.db import models
from django.utils.translation import ugettext_lazy as _
from ..choices import ACESS_SCHEME_TYPE

APP_LABEL = 'network_devices'

logger = logging.getLogger(__name__)


class NetworkDevice(models.Model):
    name = models.CharField(max_length=64, unique=True, verbose_name=_('name'))
    description = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('description'))
    ip_address = models.GenericIPAddressField(max_length=64, verbose_name=_('address'))

    class Meta:
        app_label = APP_LABEL
        ordering = ('pk',)
        verbose_name = _('Network device')
        verbose_name_plural = _('Network devices')

    def __unicode__(self):
        return '%s %s' % (self.name, self.ip_address)


class DeviceAcessScheme(models.Model):
    network_device = models.ForeignKey('NetworkDevice', db_index=True, related_name='device_acess_schemes', blank=True, null=True, verbose_name = _('network device'))
    type = models.CharField(max_length=64, choices=ACESS_SCHEME_TYPE, default=ACESS_SCHEME_TYPE.SSH, verbose_name=_('type'))
    port = models.PositiveIntegerField(blank=True, null=True, verbose_name=_('port'))
    user = models.CharField(verbose_name=_('user'), max_length=32, blank=True, null=True)
    password = models.CharField(max_length=32, blank=True, null=True, verbose_name=_('password'))
    super_password = models.CharField(max_length=32, blank=True, null=True, verbose_name=_('super Password'))

    class Meta:
        app_label = APP_LABEL
        ordering = ('pk',)
        verbose_name = _('Device Acess Scheme')
        verbose_name_plural = _('Device Acess Schemes')

    def __unicode__(self):
        return '%s %s' % (self.network_device, self.description)