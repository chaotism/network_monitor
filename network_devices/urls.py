# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url


urlpatterns = [
    url(r'^api/', include('network_devices.api.urls', namespace='api')),
]
