# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import NetworkDeviceViewSet, NetworkOperationResultViewSet

task_results = [
    url(r'^$', NetworkOperationResultViewSet.as_view({'get': 'list',}), name='list'),
]


# TODO: Возможно лучше заменить роутами?
network_devices = [
    url(r'^$', NetworkDeviceViewSet.as_view({'get': 'list', 'post': 'create'}), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', NetworkDeviceViewSet.as_view({'get': 'retrieve','put': 'update','patch': 'partial_update','delete': 'destroy'}), name='detail'),
    url(r'^(?P<pk>[0-9]+)/(?P<oper_type>[A-z]+)/$', include(task_results, namespace='task_results')),
]

urlpatterns = [
    url(r'^network_devices/', include(network_devices, namespace='network_devices')),

]

# TODO: добавить url на результаты задач /task/(?P<oper_type>[a-Z]+)/results

