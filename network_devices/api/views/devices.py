# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from django.shortcuts import Http404
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.contrib.auth.models import User, AnonymousUser
from rest_framework import generics, status, viewsets, permissions, filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from tasks.models import SubtaskResult
from ..serializers import NetworkDeviceSerializer, NetworkOperationResultSerializer
from ...models import NetworkDevice


logger = logging.getLogger(__name__)


class NetworkDeviceViewSet(viewsets.ModelViewSet):
    queryset = NetworkDevice.objects.all()
    serializer_class = NetworkDeviceSerializer
    permission_classes = (permissions.AllowAny,)  #  TODO: Поправить permissions
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('name', 'ip_address')

    # @detail_route(methods=['get'], permission_classes=[permissions.AllowAny,], url_path='change-password')  #  TODO: Поправить permissions
    # def set_password(self, request, pk=None):
    #     pass
    #
    #
    #
    # @detail_route(permission_classes=[permissions.AllowAny,], url_path='results_lists')
    # def group_names(self, request, pk=None):
    #     """
    #     Returns a list of all the group names that the given
    #     user belongs to.
    #     """
    #     user = self.get_object()
    #     groups = user.groups.all()
    #     return Response([group.name for group in groups])


class NetworkOperationResultViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SubtaskResult.objects.all()
    serializer_class = NetworkOperationResultSerializer
    permission_classes = (permissions.AllowAny,)  #  TODO: Поправить permissions
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('date', 'subtask',)

    def filter_queryset(self, queryset):
        pk = self.kwargs.get('pk')
        oper_type = self.kwargs.get('oper_type')
        return queryset.filter(subtask__task__network_device__pk=pk).filter(subtask__task__operation_type=oper_type)
