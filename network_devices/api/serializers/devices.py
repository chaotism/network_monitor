# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from rest_framework import serializers, pagination
from tasks.models import SubtaskResult
from ...models import NetworkDevice


class NetworkDeviceSerializer(serializers.ModelSerializer):
    device_acess_schemes = serializers.StringRelatedField(many=True, allow_null=True, required=False)

    class Meta:
        model = NetworkDevice
        fields = ('id', 'name', 'description', 'ip_address', 'device_acess_schemes')


class NetworkOperationResultSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubtaskResult
        fields = ('id', 'subtask', 'date', 'result', 'error')
