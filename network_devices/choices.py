# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from extended_choices import Choices
from django.utils.translation import ugettext_lazy as _


ACESS_SCHEME_TYPE = Choices(
    ('SSH', 'ssh', _('ssh')),
    ('TELNET', 'telnet', _('telnet')),
    ('HTTP', 'http', _('http')),
)

