Заготовка под проект для профилирования локальных сетей
перед использованием поставить линукс пакеты
из папки с проектом :
sudo apt-get install $(cat ./deploy/requirements/packages.list)
Сделать virtual-env:
virtualenv --no-site-packages .venv
Воспользоваться виртуальным окружением:
source .venv/bin/activate
Поставить python пакеты:
pip install -r deploy/requirements/dev-requirements.pip
Создать юзера в postgres (предварительно проверив, что установленная версия postgresql >= 9.4):
sudo -u postgres psql -c "CREATE USER kmut SUPERUSER ENCRYPTED PASSWORD 'kmut';"
Создать базу данных под проект:
sudo -u postgres psql -c "CREATE DATABASE kmut ENCODING 'UTF8' OWNER kmut;"
Провести миграции:
python ./manage.py makemigrations
python ./manage.py migrate
Сделать пользователя:
python ./manage.py createsuperuser
Запустить сервер django:
python ./manage.py runserver 0.0.0.0:8000
Запуск обработчик celery:
python ./manage.py celeryd
При эксплутации не забываем запускать на проверяемых хостах  nuttcp -S ( в будущем будет перенесено в задачи )


#Для работы с array field и json field использются нативные поля django 1.9
для их работы нужен postgresql >= 9.4


#Установка postgresql 9.4
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-9.4 pgadmin3
Если уже есть установленный postgresql не забываем смигрировать кластер.

