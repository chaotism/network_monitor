# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse


class StaticSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):  # TODO: сделать динамическим
        return ['auth:api:users:list',
                'network_devices:api:network_devices:list',
                'tasks:api:task_attributes:list',
                'tasks:api:tasks:list',
                'tasks:api:subtasks:list',]

    def location(self, object):
        return reverse(object)