# -*- coding: utf-8 -*-

"""kmut URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from __future__ import unicode_literals
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.views.generic import TemplateView
from sitemap import StaticSitemap


sitemaps = {'static': StaticSitemap}


urlpatterns = [
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name='index.html')),
]

if 'project_auth' in settings.INSTALLED_APPS:
    urlpatterns.append(url(r'^auth/', include('project_auth.urls', namespace='auth')))

if 'tasks' in settings.INSTALLED_APPS:
    urlpatterns.append(url(r'^tasks/', include('tasks.urls', namespace='tasks')))

if 'network_devices' in settings.INSTALLED_APPS:
    urlpatterns.append(url(r'^network_devices/', include('network_devices.urls', namespace='network_devices')))

if settings.STATIC_SERVE:
    from django.views.static import serve
    urlpatterns += patterns('',
                            url(r'^(?P<path>favicon\..*)$', serve,
                                {'document_root': settings.STATIC_ROOT}),
                            url(r'^%s(?P<path>.*)$' %
                                settings.MEDIA_URL[1:], serve, {'document_root': settings.MEDIA_ROOT}),
                            url(r'^%s(?P<path>.*)$' %
                                settings.STATIC_URL[1:], 'django.contrib.staticfiles.views.serve', dict(insecure=True)),
                            )